<?php
/**
 * Template name: Insertar Recurso
 */?>

 <?php 
 	if(!is_user_logged_in()){
 		header('Location:'.home_url('/login'));
 		exit();
 	}
 	//Obtener usuario con la sesión activa
 	$user = wp_get_current_user();


  if($_SERVER['REQUEST_METHOD'] == 'POST'):

      $titulo 			= $_POST['titulo'];
  		$descripcion 	=	$_POST['descripcion'];
  		$url 					= $_POST['url'];
  		$autor				= get_current_user_id();

  		$id = wp_insert_post(array
  										(
  										'post_title'	=> $titulo, 
  										'post_type'		=>'recursos', 
  										'post_content'=> $descripcion,
  										'post_author'	=> $autor,
  										'post_status' => 'pending'
  										)
  									);

  		//Actualizar campos personalizados - función plugin empleado
  		update_field( "field_578bc03571246", $url, $id );
  		subir_imagen_post($id);
      if($id):
      	wp_mail( 'yo@santipm.com', 'Nuevo Recurso', 'Mensaje' );
      endif;
  else:
  	
      
  endif;

    
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->	
	<?php wp_head(); ?>	
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?php echo home_url('/panel-principal');?>"><span><?php echo bloginfo('name'); ?> </span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">

						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php echo $user->display_name;?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								<li><a href="<?php echo home_url('/perfil');?>"><i class="halflings-icon user"></i> Profile</a></li>
								<li><a href="<?php echo wp_logout_url() ; ?>"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
				<?php $args= array(
										'menu' 			 => 'Menú panel usuario',
										'menu_class' => 'nav nav-tabs nav-stacked main-menu',
										'container'	 =>	'ul',
										'link_before'=>	'<i class="fa fa-circle"></i><span class="hidden-tablet"> ',
										'link_after' =>	'</span>',
														);

					wp_nav_menu( $args );

				if(current_user_can('manage_options' )){
					$args2= array('menu' 				=> 'Menú panel admin',
												'menu_class' 	=> 'nav nav-tabs nav-stacked main-menu',
												'container'		=>'ul',
												'link_before'	=>'<i class="fa fa-circle"></i><span class="hidden-tablet"> ',
												'link_after'	=>'</span>'
														);
					wp_nav_menu( $args2 );
				}

				?>
					
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
				<?php if($resultado){ ?>
					<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Notificación correcta</strong>
							</div>
				<?php } ?>
		
				<div class="row-fluid">
					<h2> Notificaciones </h2>
					<form class="replyForm" method="post" action="" enctype="multipart/form-data">

						<div class="control-group">
              <label class="control-label">Título</label>
              <div class="controls">
                  <input type="text" name="titulo" class="form-control" required>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Descripción</label>
              <div class="controls">
                  <textarea tabindex="3" class="input-xlarge span12" id="message" name="descripcion" rows="12" placeholder="Descripción" required></textarea>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Web Recurso</label>
              <div class="controls">
                  <input type="url"  name="url" class="form-control" required>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Imagen</label>
              <div class="controls">
                  <input type="file" name="imagen" class="form-control" required>
              </div>
            </div>
            <input type="submit" value="Enviar">

				</form>
			</div>
       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>
			
		</p>
	<?php wp_footer(); ?>
	</footer>
	

	
</body>
</html>
