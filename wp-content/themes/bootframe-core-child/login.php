<?php
/**
 * Template name: Plantilla Login
 */?>
<?php 
		 if($_SERVER['REQUEST_METHOD'] == 'POST'){
		 	//Obtener datos
		 		$creds = array();
				$creds['user_login'] = $_POST['user_login'];
				$creds['user_password'] = $_POST['user_password'];
				$creds['remember'] = false;
		 		//Comprobamos si los datos son correcto
		 		$user = wp_signon( $creds, false );
		 		
		 		if ( is_wp_error($user) ){
					$mensaje = $user->get_error_message();}
				else{
					header('Location:'. home_url('/panel-principal'));
				}
		 } ?>

<!DOCTYPE html>
<html lang="es">
<head>

		<meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
	
			<style type="text/css">
			body { background: url(<?php echo get_stylesheet_directory_uri();?>/img/bg-login.jpg) !important; }
		</style>
<?php wp_head(); ?>
</head>

<body>
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<div class="row-fluid">

					<div class="login-box">
							<?php if($mensaje){ ?>
								<div class="alert alert-danger">
									<strong>Error!</strong> Usuario/Password incorrecto.
								</div>
							<?php	} ?>
						<div class="icons">
							<a href="<?php echo home_url('/');?>"><i class="halflings-icon home"></i></a>
						</div>
						<h2><?php  _e("Login to your account"); ?></h2>
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								
								<div class="input-prepend" title="Username">
									<span class="add-on"><i class="halflings-icon user"></i></span>
									<input class="input-large span10" name="user_login" id="username" type="text" placeholder="<?php _e("type username"); ?>"/>
								</div>
								<div class="clearfix"></div>

								<div class="input-prepend" title="Password">
									<span class="add-on"><i class="halflings-icon lock"></i></span>
									<input class="input-large span10" name="user_password" id="password" type="password" placeholder="<?php _e("type password"); ?>"/>
								</div>
								<div class="clearfix"></div>
								

								<div class="button-login">	
									<button type="submit" class="btn btn-primary"><?php _e("Login"); ?></button>
								</div>
								<div class="clearfix"></div>
						</form>
						<hr>
						<h3><?php _e("Forgot password"); ?></h3>
						<p>
							<?php _e("No problem"); ?>, <a href="#"><?php _e("click here"); ?></a> <?php _e("to get a new password"); ?>.
						</p>	
					</div><!--/span-->
				</div><!--/row-->
				

			</div><!--/.fluid-container-->
		
		</div><!--/fluid-row-->
	
</body>
</html>
